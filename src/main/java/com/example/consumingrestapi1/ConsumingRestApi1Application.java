package com.example.consumingrestapi1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumingRestApi1Application {

    public static void main(String[] args) {
        SpringApplication.run(ConsumingRestApi1Application.class, args);
    }

}
