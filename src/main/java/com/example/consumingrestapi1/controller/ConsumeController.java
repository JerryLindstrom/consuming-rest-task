package com.example.consumingrestapi1.controller;


import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@RestController
public class ConsumeController {

    @GetMapping(value = "list")
    public String getCharacter () throws IOException {
        URL url = new URL("https://anapioficeandfire.com/api/houses/378");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        String result = null;
        if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
            InputStreamReader reader = new InputStreamReader(connection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;
            StringBuffer responseContent = new StringBuffer();

            while ((line = bufferedReader.readLine()) != null) {
                responseContent.append(line);
            }

            bufferedReader.close();

            JSONObject houseTargaryen = new JSONObject(responseContent.toString());

            result = houseTargaryen.getJSONArray("titles").toString(4);
        }
        return result;
    }
}
